# Contoh Membuat Laporan Di Pemrograman Java Menggunakan IReport
Contoh Program Java Untuk Mencetak Laporan Menggunakan IReport

## Persiapan
1. Apache Netbeans 12
2. PostgreSQL 14
3. Java 8
4. IReport 5.6.0

## Database
* Membuat Database 
```
createdb -U postgres db_mahasiswa
```
* Membuat Tabel
```
CREATE TABLE m_mahasiswa(
  id_mahasiswa bigserial,
  nim VARCHAR(255),
  nama VARCHAR(255),
  ttl VARCHAR(255),
  jurusan VARCHAR(255),
  alamat VARCHAR(255)
);
```
* Menambahkan Data Mahasiswa
```
INSERT INTO m_mahasiswa(id_mahasiswa, nim, nama, ttl, jurusan, alamat) VALUES 
(1, '41101328', 'Akira', 'Indramayu, 27 Juli 1990', 'Teknik Informatika', 'Jln. Kavling Pemda 2 No. 269');
INSERT INTO m_mahasiswa(id_mahasiswa, nim, nama, ttl, jurusan, alamat) VALUES (2, '41101390', 'Akbar', 'Cirebon, 26 September 1990', 'Teknik Informatika', 'Kel. Gumulung Lebak - Greged');
INSERT INTO m_mahasiswa(id_mahasiswa, nim, nama, ttl, jurusan, alamat) VALUES (3, '32102438', 'Dendi', 'Indramayu, 03 Maret 1995', 'Management Informatika', 'Blok Balaidesa No. 49 - Karangampel');
INSERT INTO m_mahasiswa(id_mahasiswa, nim, nama, ttl, jurusan, alamat) VALUES (4, '31115419', 'Luqman', 'Indramayu, 17 Juli 1997', 'Komputerisasi Akuntansi', 'Blok Balaidesa No. 50 - Karangampel');
INSERT INTO m_mahasiswa(id_mahasiswa, nim, nama, ttl, jurusan, alamat) VALUES (5, '41128527', 'Hana', 'Cikampek, 09 Juli 1994', 'Teknik Informatika', 'Kel. Gumulung Lebak - Greged');
```
### Catatan
Untuk lebih jelasnya silahkan kunjungi artikel berikut ->
[Link Artikel](https://www.community-java.com/2019/02/cara-membuat-report-di-java-dengan-jasperreport.html)