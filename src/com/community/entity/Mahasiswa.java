package com.community.entity;

/**
 *
 * @author Java Community
 */
public class Mahasiswa {
    private long id;
    private String nim;
    private String nama;
    private String ttl;
    private String jurusan;
    private String alamat;

    public Mahasiswa() {
    }

    public Mahasiswa(long id, String nim, String nama, String ttl, 
            String jurusan, String alamat) {
        this.id = id;
        this.nim = nim;
        this.nama = nama;
        this.ttl = ttl;
        this.jurusan = jurusan;
        this.alamat = alamat;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    @Override
    public String toString() {
        return nim;
    }
}
