package com.community.controller;

import com.community.entity.Mahasiswa;
import com.community.sql.MahasiswaSQL;
import com.community.sql.MahasiswaSQLImpl;
import com.community.view.ViewPrinting;
import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
/**
 *
 * @author Java Community
 */
public class ControllerPrinting {
    private final ViewPrinting view;
    private List<Mahasiswa> list = new ArrayList<>();

    public ControllerPrinting(ViewPrinting view) {
        this.view = view;
        getDataMahasiswa();
    }
    
    private void getDataMahasiswa(){
        try {
            MahasiswaSQL sql = new MahasiswaSQLImpl();
            list = sql.getDataMahasiswa();
            
            for(Mahasiswa obj : list){
                view.getModel().addRow(new Object[]{
                    obj, obj.getNama(), obj.getTtl(), obj.getAlamat(), obj.getJurusan()
                });
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Gagal Mengambil Data Mahasiswa");
        }
    }
    
    public void cetakLaporan(){
        try {            
            String path = "src/com/community/printing/laporan.jrxml";
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
            File file = new File(path);
            JasperDesign jasperDesign = JRXmlLoader.load(file);
            JasperReport report = JasperCompileManager.compileReport(jasperDesign);
            JasperPrint print = JasperFillManager.fillReport(report, null, dataSource);
            JasperViewer.viewReport(print, false);
        } catch (JRException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Gagal Mencetak Laporan");
        }
    }
}