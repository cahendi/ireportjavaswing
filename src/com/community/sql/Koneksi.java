package com.community.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Java Community
 */
public class Koneksi {
    public static Connection getKoneksi() {
        Connection koneksi = null;
        try {
            String driver = "org.postgresql.Driver";
            String url = "jdbc:postgresql://192.168.1.23:7117/db_mahasiswa";
            String user = "postgres";
            String password = "postgres";
            
            Class.forName(driver);
            koneksi = DriverManager.getConnection(url, user, password);

        } catch (ClassNotFoundException | SQLException ex) {
            ex.printStackTrace();
        }
        
        return koneksi;
    }
}
