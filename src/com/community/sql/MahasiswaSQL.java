package com.community.sql;

import com.community.entity.Mahasiswa;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Java Community
 */
public interface MahasiswaSQL {
    public List<Mahasiswa> getDataMahasiswa() throws SQLException;
}
