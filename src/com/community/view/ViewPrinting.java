package com.community.view;

import com.community.controller.ControllerPrinting;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Java Community
 */
public class ViewPrinting extends JFrame implements ActionListener{
    
    private final ControllerPrinting cp;
    private JTable tabel;
    private DefaultTableModel model;
    private JButton cetak;
    
    public ViewPrinting() {
        userInterface();
        cp = new ControllerPrinting(this);
    }
    
    private void userInterface(){
        setTitle("Contoh Printing Jasper Report");
        setSize(1000, 500);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setResizable(false);
        
        JPanel panelHeader = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        
        JLabel header = new JLabel("Data Mahasiswa");
        header.setHorizontalAlignment(JLabel.CENTER);
        header.setVerticalAlignment(JLabel.CENTER);
        header.setFont(new Font("Arial", Font.PLAIN, 20));
        panelHeader.add(header);
        
        getContentPane().add(panelHeader, BorderLayout.NORTH);
        
        model = new DefaultTableModel();
        model.setColumnIdentifiers(new Object[]{
            "NIM", "Nama", "Tempat, Tanggal Lahir", "Alamat", "Jurusan"
        });
        
        tabel = new JTable(model);
        getContentPane().add(new JScrollPane(tabel));
        
        JPanel panelButton = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        
        cetak = new JButton("Cetak");
        cetak.addActionListener(this);
        panelButton.add(cetak);
        
        getContentPane().add(panelButton, BorderLayout.SOUTH);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==cetak){
            cp.cetakLaporan();
        }
    }
    
    public DefaultTableModel getModel() {
        return model;
    }

}
